#include"m_chat_header.h"
//#include"Resolve_Packet.c"
//server program
#define BUFLEN 1024

void Receive_Message(char *packet,char *recv_from)
{
	online *temp=online_users_permenant_list;
	for(temp=online_users_permenant_list;temp!=NULL;temp=temp->next){
	//for(int i=0;i<ONLINE_COUNT;i++)
	//{
		if(strcmp(recv_from,temp->ip_addr)==0)
		{
			printf("[%s]-",temp->host_name);
		}
	}		
			int i=0,count=0;
			for(i=0;i<strlen(packet);i++)
			{
				if(packet[i]=='\0' || packet[i]==' '){
					count++;
					}
				if(count==3)
					break;
			}
			for(int j=i;j<strlen(packet);j++)
				printf("%c",packet[j]);
			printf("\n");
}

//Packet TYPE
/*	1	-	Request send packet type
	2	-	Response for request packet type
	3	-	Send messages to other machines
	4	-	Messages acknowledgement packet type
	5	-	Sends all connected ip address data sepereted byspace
*/
void * Resolve_Packet(void * vargp)
{
	char packet[1024],recv_from[32];
	strcpy(packet,RECV_PACKET);
	strcpy(recv_from,RECV_FROM);
	
 if(strcmp(recv_from,MYIP_SERVER)!=0)
 {
	//printf("Resolve Host %s=%s\n",recv_from,packet);
 	char own_dest_ip[50];
 	int i=0;
 	for(i=0;i<1024;i++)
 	{
 		if(packet[i]==' ' || packet[i]=='\0')
 		{
 			own_dest_ip[i]='\0';
 			break;
 		}
 		//printf("Copy");
 		own_dest_ip[i]=packet[i];
 	}
 	i++;//go to type of packet after dest ip address
 	//printf("\nAfter get ip finder [%s][TYPE -%c][MYIP-%s]\n",own_dest_ip,packet[i],MYIP);
 	if(strcmp(own_dest_ip,MYIP)==0)
 	{
		//printf("\nFrom Other Host TYPE-%c\n",packet[i]); 	
		if(packet[i]=='1')
		{//request for online or not
			Send_True_Response_Available(packet,recv_from);
		}		
		else if(packet[i]=='2')
		{
			Get_True_Response_Available(packet,recv_from);
		}
		else if(packet[i]=='3')
		{
			Receive_Message(packet,recv_from);
		}
		else if(packet[i]=='4')
		{
			Acknowledgement_Receive(packet,recv_from);
		}
		else
			Packet_Currupted(packet,recv_from);
	} 
	else//packet not for current machine then
 	{//just forward it
 		Forward_Packet(packet,recv_from);
 	}
 }	
}

void * Server(void *vargp)
{
	//this is like server of socket
	char recv_packet[BUFLEN];
	int ser_sockfd,cli_sockfd;
	int ser_len,cli_len;
	int recv_len;
	struct sockaddr_in ser_address;
	struct sockaddr_in cli_address;
	cli_len=sizeof(cli_address);
	unlink("server_socket");
	ser_sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if(ser_sockfd==-1){
		printf("Socket not create");
		return NULL;
		}
	//Resolve_Packet(recv_packet);	 
	bzero(&ser_address,sizeof(ser_address));
	ser_address.sin_family=AF_INET;
	ser_address.sin_addr.s_addr=inet_addr(MYIP_SERVER);
	ser_address.sin_port=htons(PORT);
	if((bind(ser_sockfd,(struct sockaddr *)&ser_address,sizeof(ser_address)))!=0)
	{
		printf("Error. Socket Not Bind Successful.");
		exit(0);
	}
		listen(ser_sockfd,55);
		//waits for incomming connection

	//thread obj create
	pthread_t tid;
	while(1)
	{		 
		fflush(stdout);	
		
		//printf("Waiting for packet recv...\n");
		int flag=recvfrom(ser_sockfd, recv_packet , BUFLEN, 0, (struct sockaddr *) &cli_address, &cli_len);
		if(flag>0)
		{
			//printf("packet-%s\n",recv_packet);

			char recv_from[30];
			strcpy(recv_from,inet_ntoa(cli_address.sin_addr));


			strcpy(RECV_PACKET,recv_packet);
			strcpy(RECV_FROM,recv_from);
			pthread_create(&tid,NULL,Resolve_Packet,NULL);
			//purpose to create thread is server must in listen mode  always to next packet			
			
			//Resolve_Packet(recv_packet,recv_from);
			//printf("\nAfter Resolve Host\n");
			//Clean_Show();
			for(int i=0;i<1024;i++)
				recv_packet[i]='\0';
		}
	}
	close(ser_sockfd);
	return NULL;
}
