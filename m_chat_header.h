#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<fcntl.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<sys/types.h>
#include <pthread.h>

//current user name
char MYNAME[50];

//for temp use
char RECV_PACKET[1024];
char RECV_FROM[32];

//fetch ip address auto using system call
//char MYIP[15]={"192.168.43.129"};
//char MYIP_SERVER[15]={"192.168.43.129"};

//temparory ip store
char MYIP[40];//={"192.168.43.76"};
char MYIP_SERVER[40];//={"192.168.43.76"};


int old_count;//for no of users online check

//check for ip address is online or not
#define PORT 7805

//This port for only send request for communicate
#define check_ip_for_online "m_check_ip.txt"

//Header file for Chat
char ipaddress[32]="192.168.";



//structure for whose online 
typedef struct Online{
	char ip_addr[32];
	char host_name[100];
	struct Online *next,*prev;
}online;

//Whose neighbour. own inside each node whose their connected
typedef struct Neighbour
{
	char ip_addr[32];
	struct Online *connected;
	struct Neighbour *next;
}neighbour;

online *online_users_permenant_list=NULL;

//create new online node
online *newOnline()
{
	online *temp;
	temp=(online *)malloc(sizeof(online));
	temp->next=temp->prev=NULL;
	return temp;
}

///Add last into list
online *addLast(online *n,online *temp)
 {
	if(n==NULL)
	{
			//printf("[[Add Last]]");
			return temp;
	}
	online *temp1;
	temp1=n;
	while(temp1->next!=NULL)
	{
		temp1=temp1->next;
	}
	return n;
 }

//Calc length in list
int Length(online *t)
 {
	if(t==NULL)
		return 0;
	else
		return 1+Length(t->next);
 }

//recent ip address list
//online users count //acess for all users.
int ONLINE_COUNT=0;
char ONLINE_IP[100][32];
char ONLINE_NAME[100][32];

//keep history "chat hostory" or whose connected what time
typedef struct History{
	char ip_addr[32];
	char time[20];
	char host_name[20];
	struct History *next,*prev;
}history;

//if some user goes offline then this function remove it from online list
void GO_OFFLINE_USER_UPDATE(online *node)
{
//	printf("\nGo offline USER UPDATE Func\n");
	online *temp=online_users_permenant_list;
	if(strcmp(temp->ip_addr,node->ip_addr)==0)
		online_users_permenant_list=online_users_permenant_list->next;
	else
	{
		for(temp=online_users_permenant_list;temp->next!=NULL;temp=temp->next)
		{
			if(strcmp(temp->next->ip_addr,node->ip_addr)==0)
			{
				temp->next=temp->next->next;
				break;
			}
		}
	}
	//remove node in online list
}

//check whose recent online for update online
void Check_Recent_to_Perment_Users()
{
	online *temp=NULL;
	if(online_users_permenant_list!=NULL)
		temp=online_users_permenant_list;
//	printf("\nCheck Recent to premenant users\n");
	while(temp!=NULL)
	{
//		printf("\nin recent ");
		int flag=0;
		for(int i=0;i<ONLINE_COUNT;i++)
		{
			if(strcmp(temp->ip_addr,ONLINE_IP[i])==0)
			{
				flag=1;
				ONLINE_IP[i][0]='\0';
			}
		}
		if(flag==0)
		{
//			printf("Remove User- %s",temp->ip_addr);
			GO_OFFLINE_USER_UPDATE(temp);
		}
		temp=temp->next;
	}//while end
	for(int i=0;i<ONLINE_COUNT;i++)
	{
		if(ONLINE_IP[i][0]!='\0')
		{//means that user new add in recent
//			printf("\nnew user add list\n");
			online *newUser=NULL,*temp1=NULL;
			newUser=newOnline();
			strcpy(newUser->host_name,ONLINE_NAME[i]);
			strcpy(newUser->ip_addr,ONLINE_IP[i]);
			if(online_users_permenant_list==NULL)
				online_users_permenant_list=newUser;
			else
			{
				online *temp=online_users_permenant_list;
				while(temp->next!=NULL)
					temp=temp->next;
				temp->next=newUser;
			}
			temp1=online_users_permenant_list;
//			printf("[Length=%d]\n",Length(temp1));
		}
	}
	//Update online count
	ONLINE_COUNT=0;
}


//I/P-	Formatted packet means string that contains all info
//O/P-	return 1 when successfully send packet
int Send_Packet(char *packet,char *dest_ip,int size)
{
	int result,len,sockfd;//Socket file discriptor
	struct sockaddr_in cli_address,ser_address;
	//sock_dgram-UDP 
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd==-1)
	{
		printf("Client socket not create");
		return -1;
	}
	//make zero '\0' memory provided address and size
	bzero(&ser_address,sizeof(ser_address));
	//IPV4 family
	ser_address.sin_family=AF_INET;
	ser_address.sin_addr.s_addr=inet_addr(dest_ip);
	ser_address.sin_port=htons(PORT);
	int send_bytes=sendto(sockfd,packet,size,0,(struct sockaddr *)&ser_address,sizeof(struct sockaddr));
	//if(send_bytes>0)
		//printf("%d %s\n",send_bytes,dest_ip);	
	close(sockfd);
	return send_bytes;
}


int Check_Already_User_Present_Online(char *ip)
{
	for(int i=0;i<ONLINE_COUNT;i++)
	{
		if(strcmp(ONLINE_IP[i],ip)==0)
		{
			//add online user recent online
			
			return 1;
			
		}
	}
	return 0;
}


void ADD_USER_ONLINE_LIST(char *packet,char *recv_from)
{
//	printf("\nADDING USER PLEASE WAIT...-\n");
	int flag=0,j=0,count=0,i=0;
	char name[50];
	for(i=strlen(packet)-2;i>=0;i--)
	{
		if(packet[i]==' ')
			break;
	}
	for(int k=i;k<=strlen(packet);k++)
		{
			name[j]=packet[k];
			j++;
		}
	name[j++]='\0';
//	printf("\nUSER ADD=%s\n",name);
	if(Check_Already_User_Present_Online(recv_from)==0)
	{
		strcpy(ONLINE_IP[ONLINE_COUNT],recv_from);
		strcpy(ONLINE_NAME[ONLINE_COUNT],name);
		ONLINE_COUNT++;
		//printf("Count-%d\n",ONLINE_COUNT);
/*
		online *temp=newOnline();
		strcpy(temp->host_name,name);
		strcpy(temp->ip_addr,recv_from);
		online_list=addLast(online_list,temp);
		//printf("No of Users Online-%d\n",Length(online_list));
*/
	}
	return;
}

void Online_List()
{
	printf("Online USERS\nSr.\tName\t\tIP\n");
	int flag=0;

	online *temp;
	temp=online_users_permenant_list;
	int c=0;
	for(temp=online_users_permenant_list;temp!=NULL;temp=temp->next)
	{
		//printf("Printing users Online List func\n");
		printf("%d\t%s\t\t%s\n",c+1,temp->host_name,temp->ip_addr);
		c++;
		flag=1;
	}

/*	for(int i=0;i<ONLINE_COUNT;i++)
	{
		flag=1;
		printf("%d\t%s\t\t%s\n",i+1,ONLINE_NAME[i],ONLINE_IP[i]);
	}
*/
	if(flag==0)
		printf("No Users Online\n");
}

//clear the screen and provide new blank space to print
void Clean_Show()
{
	system("clear");
	Online_List();
	printf("\n\t\t\tWrite Messages\n");
}

//TYPE-1 "send back response true"
void Send_True_Response_Available(char *recv_packet,char *recv_from)
{
	char send_packet[100],type[10]={'2'},space[10]={" "};
//	printf("\nUser request arrive. Sending true response-%s\n",recv_from);
	strcpy(send_packet,recv_from);
	strcat(send_packet,space);
	strcat(send_packet,type);
	strcat(send_packet,space);
	strcat(send_packet,MYIP);
	strcat(send_packet,space);
	strcat(send_packet,MYNAME);
	strcat(send_packet,space);
	//printf("\nPacket-[%s]\n\n",send_packet);	

	Send_Packet(send_packet,recv_from,strlen(send_packet));

	//	printf("True Response Send\n");
	//add it to online

	ADD_USER_ONLINE_LIST(recv_packet,recv_from);
}

//TYPE-2
void Get_True_Response_Available(char *packet,char *recv_from)
{
	//Add online list user
	//	printf("\nSuccesssfully response get from [%s].\n",recv_from);
	//after user add online list send request for connected all ip address wants using packet type "%"
	
	ADD_USER_ONLINE_LIST(packet,recv_from);

	//printf("After Added\n");
}

//TYPE-4
void Acknowledgement_Receive(char *packet,char *recv_from)
{

}

//Rather than type
void Packet_Currupted(char *packet,char *recv_from)
{
	
}

//Forward packet to dest ip
void Forward_Packet(char *packet,char *recv_from)
{

}

//fetch current system ip address by using hoatname --all-ip-address system call
void MYIP_a()
{
  FILE *fp,*outputfile;
  char currentIP[40];

  fp = popen("hostname --all-ip-address", "r");
  fgets(currentIP, sizeof(currentIP), fp); 
    
	strcpy(MYIP,currentIP);

	strcpy(MYIP_SERVER,currentIP);
    
	MYIP[strlen(MYIP)-2]='\0';
	MYIP_SERVER[strlen(MYIP_SERVER)-2]='\0';
	pclose(fp);

  //outputfile = fopen("text.txt", "a");
  //fprintf(outputfile,"%s\n",var);
  //fclose(outputfile);

}

//rage of ip address write in a file for purpose of ping
void write_IP_FILE()
{
	FILE *fp;
	fp=fopen(check_ip_for_online,"w+");
	int cnt=0;
	for(int third=1;third<=255;third++)
	{
		for(int fourth=1;fourth<=254;fourth++)
		{
			if(third==1 && fourth==62)//own ip does not search
				fourth++;
			char newIP_3[5],newIP_4[5],search_IP[32];
			sprintf(newIP_3,"%d",(unsigned int)third);	//convert integer PID to an character array using sprintf
			sprintf(newIP_4,"%d",(unsigned int)fourth);	//convert integer PID to an character array using sprintf
			strcat(newIP_3,".");
			newIP_4[3]='\0';
			strcpy(search_IP,ipaddress);
			strcat(search_IP,newIP_3);
			strcat(search_IP,newIP_4);

				fprintf(fp,"%s\n",search_IP);
			
			cnt++;
		}
	}
//	printf("Total _IP-%d ",cnt);
	fclose(fp);
}
