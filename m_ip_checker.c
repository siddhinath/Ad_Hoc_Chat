//#include"chat_header.h"
#include"m_Recv_Packet.c"
//it sends small packet to each ip address to check ip address are reachable or not

//it check all ip address and update online list whose online
void * Check_Whose_Online(void *vargp)
{
	int result,len,sockfd;//Socket file discriptor
	struct sockaddr_in cli_address,ser_address;

	//sock_DGRAM-UDP 
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd==-1)
	{
		printf("Client socket not create");
		//return -1;
	}

//	printf("\nChecking all ip address rechable or not\n");
	FILE *fp;
	fp=fopen(check_ip_for_online,"r");
	if(fp==NULL){
		printf("File not exist");
		exit(0);
	}
	char ip[32],packet[1000],Type[10]={'1'};
	//strcpy second argument must be array type not and varible type
	while(!feof(fp))
	{
		fscanf(fp,"%s",ip);
		if(feof(fp))
		{
			fseek(fp,0,SEEK_SET);
			//checking whose recent online for update list

//			printf("Change file pointer FSEEK\n");
			if(old_count!=ONLINE_COUNT)
			{
				old_count=ONLINE_COUNT;
			}
			old_count=ONLINE_COUNT;
			Clean_Show();
			Check_Recent_to_Perment_Users();
			//sleep(3);//wait for 5 sec to next check
			//break;
		}
		//printf("%s\n",ip);
		strcpy(packet,ip);
		strcat(packet," ");
		//printf("%d ",strlen(packet));	
		strcat(packet,Type);
		strcat(packet," ");
		//printf("%d ",strlen(packet));
		strcat(packet,MYIP);
		strcat(packet," ");
		strcat(packet,MYNAME);
		//printf("Send Ready-%s\n",ip);
		//int result=Send_Packet(packet,ip,strlen(packet));	
	//make zero '\0' memory provided address and size
	bzero(&ser_address,sizeof(ser_address));
	//IPV4 family
	ser_address.sin_family=AF_INET;
	ser_address.sin_addr.s_addr=inet_addr(ip);
	ser_address.sin_port=htons(PORT);
	int send_bytes=sendto(sockfd,packet,strlen(packet),0,(struct sockaddr *)&ser_address,sizeof(struct sockaddr));
	}		
	fclose(fp);
}

//send message to other users
void Send_Message(char *data,char *name)
{
	char ip[32],packet[1024],Type[10]={'3'},space[10]={" "};
	online *temp=NULL;
	for(temp=online_users_permenant_list;temp!=NULL;temp=temp->next){
	//for(int i=0;i<ONLINE_COUNT;i++)
	//{
	//printf("Send-%s\n",ONLINE_NAME[i]);

		strcpy(ip,temp->ip_addr);
		strcpy(packet,ip);
		strcat(packet,space);

		strcat(packet,Type);
		strcat(packet,space);

		strcat(packet,MYIP);
		strcat(packet,space);

		strcat(packet,data);
		//printf("Sending %s",packet);
		int result=Send_Packet(packet,ip,strlen(packet));
		//printf("\nSend-To ip-%s\n",ip);
	}
}

//it checks current online users and update list if changed
void * check_Current_Online_Status(void *vargp)
{
	int count=old_count;
	while(1){		
		if(old_count!=count){
			Clean_Show();
			count=old_count;
			}//if new user available in network
		}
}

//provide user interface with chat program.
void Writer()
{
	char write_data[1024];
	int count=ONLINE_COUNT;
	//Online_List();
	while(1)
	{
		printf("\n");
		char name[30];
		scanf("%s",name);
		fgets(write_data,1024,stdin);

		//Send_Packet(write_data,cli_ip,strlen(write_data));
		Send_Message(write_data,name);
		for(int i=0;i<1024;i++)
			write_data[i]='\0';
	}
}


//main driver program
int main(int argc,char *argv[])
{
	if(argc<2)
	{
		printf("\nPlease provide USER NAME as command line argument.\n");
		exit(0);
	}

	write_IP_FILE();
	MYIP_a();
	//user name registered using command line argument
	strcpy(MYNAME,argv[1]);

	//write_IP_FILE();

	//thread instance create in array	
    pthread_t tid[3];

	//call Server For packet accept function using thread ....
    pthread_create(&tid[0], NULL,Server, NULL);

    //pthread_join(tid[0], NULL);//waiting for thread complete execution
    //printf("After Thread\n");

	//Check whose online call fun using thread it recheck every 10 sec whose online or not
	pthread_create(&tid[1],NULL,Check_Whose_Online,NULL);
	//Check_Whose_Online();

//	printf("\nRedy for Message Write.Please use Online ID Provided\n");
	//system("clear");
	Online_List();
	
	pthread_create(&tid[2],NULL,check_Current_Online_Status,NULL);
	Writer();

	return 0;
}
